# Exercises

Note: You can find completed exercises in the `CompletedExercises` folder

### Exercise 1

1. Write an infix function named `~=` that takes two Strings and returns a Bool if the first letter of each string is the same
2. Call your new `~=` function on 2 strings of your choosing and display the results on the page.

Hint: You're going to need some functions from the String Module http://package.elm-lang.org/packages/elm-lang/core/4.0.0/String



### Exercise 2


1. Call your new function `~=` as a prefix function.


### Exercise 3

1. Using function composition, create a function named `wordCount` that returns the number of words in a sentence.
