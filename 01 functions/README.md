# Exercises

### Initial Setup

You need to install the dependancies by running the following two commands
```
elm package install
npm install
```

Create a new elm file named

### Exercise 1

Complete in the following file: `Exercise1.elm`

1. Import the Html Module
2. Show your name on the page

### Exercise 2

Complete in the following file: `Exercise2.elm`

1. Import the Html Module
2. Create a function that capitalizes names longer than 10 characters
3. Display the name in appropriate capitalized form with the length of the name right next to it like the following: "James Moore - name length: 11"

Hint: You're going to need some functions from the String Module http://package.elm-lang.org/packages/elm-lang/core/4.0.0/String
